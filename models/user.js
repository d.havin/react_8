import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
const { Schema } = mongoose;

const userSchema = new Schema(
  {
    name: { type: String, require: true, default: "defaultName" },
    surname: { type: String, require: true, default: "defaultSurname" },

    email: {
      type: String,
      require: true,
      unique: true,
    },
    password: {
      type: String,
      require: true,
    },
    role: {
      type: String,
      default: "user",
    },
    cart: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "cart",
      autopopulate: true,
    },
    orders: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "order",
        autopopulate: true,
      },
    ],
  },
  { versionKey: false }
);

userSchema.plugin(autopopulate);
const User = mongoose.model("user", userSchema);

export { userSchema, User }; 
