import {
  requestCreateUser,
  requestCreateUserSuccess,
  requestCreateUserFailed,

  requestdeleteUser,
  requestdeleteUserSuccess,
  requestdeleteUserFailed,

  requestUser,
  requestUserSuccess,
  requestUserError,
} from "../actions/user.action";
import { put, call, takeEvery } from "redux-saga/effects";

//====================================================================getAllUsers
// генератор - наблюдатель
export function* watchFetchUser() {
  yield takeEvery("FETCHED_USER", fetchUserAsync )
  yield takeEvery("FETCH_CREATE_USER", fetchCreateUserAsync )
  yield takeEvery("FETCH_DELETE_USER", fetchDeleteUserAsync )
}

// worker-saga
function* fetchUserAsync() {
  try {
    console.log("Hello get")
    yield put(requestUser());
    const data = yield call(() => {
      return fetch("http://localhost:3030/user/allUsers", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
        },
      }).then((res) => res.json());
    },
    );
    yield put(requestUserSuccess(data));
  } catch (error) {
    yield put(requestUserError());
  }
} 
//============================================================CREATE User

// worker-saga
function* fetchCreateUserAsync(action) {
  try {
    console.log("Hello create")
    yield put(requestCreateUser());
    const data = yield call((data) => {                          // <=?
      return fetch("http://localhost:3030/user/create", {
        method: "POST",
        body: JSON.stringify(action.mongoData),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
        },
      })
    });
    yield put(requestCreateUserSuccess(action.mongoData));
  } catch (error) {
    yield put(requestCreateUserFailed());
  }
} 

//======================================================DELETE User

function* fetchDeleteUserAsync(action) {
  try {
    yield put(requestdeleteUser());
    
    const {data} = action;
    console.log(data)
    const dataM = yield call(() => {
      return fetch(`http://localhost:3030/user/delete/${action.data}`, {    // <=========== ??
        method: "DELETE",
        body: JSON.stringify(data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
        },
      })
    });
    yield put(requestdeleteUserSuccess(data));
  } catch (error) {
    yield put(requestdeleteUserFailed());
  }
}   
