import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import Navbar from "./components/NavbarPage";
import ProfilePage from './components/ProfilePage'
import {store} from './store/store'
import {Provider} from "react-redux";

const App: React.FC = () => {
  return (
    <div className="App">
      <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <ProfilePage/>
      </BrowserRouter>
      </Provider>
    </div>
  );
};

export default App;
