import {
  REQUEST_CREATE_USER,
  REQUEST_CREATE_USER_SUCCEEDED,
  REQUEST_CREATE_USER_FAILED,
  REQUEST_DELETE_USER,
  REQUEST_DELETE_USER_SUCCEEDED,
  REQUEST_DELETE_USER_FAILED,
  REQUESTED_USER,
  REQUESTED_USER_SUCCEEDED,
  REQUESTED_USER_FAILED,
} from "../constants/user.constants";

const initialState = {
  mongoData: [],
  loading: false,
  error: false,
};

const profileReducer = (state = initialState, action: any) => {
  switch (action.type) {
    // ======================================= CREATE
    case REQUEST_CREATE_USER:
      return {
        mongoData: [],
        loading: true,
        error: false,
      };
    case REQUEST_CREATE_USER_SUCCEEDED:
      return {
        mongoData: [action.mongoData],
        loading: false,
        error: false,
      };
    case REQUEST_CREATE_USER_FAILED:
      return {
        mongoData: [],
        loading: false,
        error: true,
      };
    //===================================DELETE
    case REQUEST_DELETE_USER:
      return {
        mongoData: [],
        loading: true,
        error: false,
      };
    case REQUEST_DELETE_USER_SUCCEEDED:
      return {
        mongoData: action.mongoData,      //.filter((user:any) => user._id !== action._id),   // <====
      };
    case REQUEST_DELETE_USER_FAILED:
      return {
        mongoData: [],
        loading: false,
        error: true,
      };
    // ======================================GET_ALL
    case REQUESTED_USER:
      return {
        mongoData: [],
        loading: true,
        error: false,
      };
    case REQUESTED_USER_SUCCEEDED:
      return {
        mongoData: action.mongoData,
        loading: false,
        error: false,
      };
    case REQUESTED_USER_FAILED:
      return {
        mongoData: [],
        loading: false,
        error: true,
      };
    default:
      return state;
//============================ EDIT
    // case EDIT_USER:
    // let newState: [] = []
    // state.mongoData.forEach(el=>{
    //     if(el.id === action.data.id){
    //         newState.push(action.data)
    //     }else{
    //         newState.push(el)
    //     }
    // });
    // return newState
  }
};

export default profileReducer;
