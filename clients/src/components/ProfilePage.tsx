import React, { useState } from 'react';
// import '../styles/profilePage.css';
import { fetchCreateUser, editUser, fetchDeleteUser, fetchUser, } from "../actions/user.action";
import { connect } from "react-redux";
import ProfileInput from './ProfileInput.component';

const ProfileInfo = ( props: any ) => {
    const [editedName, setName] = useState('');
    const [editedSurName, setSurName] = useState('');
    const [editedEmail, setEmail] = useState('');
    const [editedPassword, setPassword] = useState('');
    // const [data, setData] = useState('')

    const handleCreate = () => {
        let user = {
            name: editedName,
            surname: editedSurName,
            email: editedEmail, 
            password: editedPassword
        }
        props.fetchCreateUser(user);
        setName('');
        setSurName('');
        setEmail('');
        setPassword('');
    }

    const handleEdit = () => {
        let user = {
            name: editedName,
            surName: editedSurName,
            email: editedEmail,
            password: editedPassword
        }
        props.editUser(user);

        setName('');
        setSurName('');
        setEmail('');
        setPassword('');
    }

    const handleDelete = (user:any) => {
        props.fetchDeleteUser(user._id)
        // console.log(user)
        // console.log(user._id)
    }

    const handleList = () => {
        props.fetchUser()
    }

    return (
        <div className="editInfoMenu">
            Имя: <ProfileInput data={editedName} setInputData={setName} />
            Фамилия: <ProfileInput data={editedSurName} setInputData={setSurName} />
            Email: <ProfileInput data={editedEmail} setInputData={setEmail}/>
            Пароль: <ProfileInput data={editedPassword} setInputData={setPassword} />
            <button className="profButton" onClick={() => handleCreate()}>Создать</button>
            <button className="profButton" onClick={() => handleList()}>Список</button>
            {props.profile.loading     
                ? <p>Loading...</p> 
                : props.profile.error
                    ? <p>Error, try again</p>
                    : props.profile.mongoData.map((user: any) => ( 
                        <ul>
                            <li>
                                {user.name}
                                {/* {console.log(user)} */}
                                <button className="profButton" onClick={() => handleEdit()}>Изменить</button>
                                <button className="profButton" onClick={() => handleDelete(user)}>Удалить</button>
                            </li>
                        </ul>
                    ))
            }
            
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    profile: state.profile
});

const mapDispatchToProps = (dispatch: any) => ({
    fetchCreateUser: (data: any) => dispatch(fetchCreateUser(data)),
    // editUser: (data:any) => dispatch(editUser(data)),
    fetchDeleteUser: (data: any) => dispatch(fetchDeleteUser(data)),
    fetchUser: () => dispatch(fetchUser())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileInfo);

