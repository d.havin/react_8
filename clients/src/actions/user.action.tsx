import {
  REQUEST_CREATE_USER,
  REQUEST_CREATE_USER_SUCCEEDED,
  REQUEST_CREATE_USER_FAILED,
  FETCH_CREATE_USER,

  EDIT_USER,

  REQUEST_DELETE_USER,
  REQUEST_DELETE_USER_SUCCEEDED,
  REQUEST_DELETE_USER_FAILED,
  FETCH_DELETE_USER,

  REQUESTED_USER,
  REQUESTED_USER_SUCCEEDED,
  REQUESTED_USER_FAILED,
  FETCHED_USER,
} from "../constants/user.constants";

export const requestCreateUser = () => ({ type: REQUEST_CREATE_USER});
export const requestCreateUserSuccess = (data: any) => ({ type: REQUEST_CREATE_USER_SUCCEEDED, data });
export const requestCreateUserFailed = () => ({ type: REQUEST_CREATE_USER_FAILED });
export const fetchCreateUser = (mongoData: any) => ({ type: FETCH_CREATE_USER, mongoData });

export const editUser = (data: any) => ({ type: EDIT_USER, data });

export const requestdeleteUser = () => ({ type: REQUEST_DELETE_USER });
export const requestdeleteUserSuccess = (data: any) => (console.log(data), { type: REQUEST_DELETE_USER_SUCCEEDED, data });
export const requestdeleteUserFailed = () => ({ type: REQUEST_DELETE_USER_FAILED });
export const fetchDeleteUser = (data: any) => (console.log(data), { type: FETCH_DELETE_USER, data });

export const requestUser = () => {return { type: REQUESTED_USER }};
export const requestUserSuccess = (data: any) => {return { type: REQUESTED_USER_SUCCEEDED, mongoData: data.users}};
export const requestUserError = () => {return { type: REQUESTED_USER_FAILED }};
export const fetchUser = () => {return { type: FETCHED_USER }};
