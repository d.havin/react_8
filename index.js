import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import { userRouter } from './routs/index.js'; // authRouter,  categoryRouter, cartRouter, productRouter, orderRouter
// import { authenticateJWT } from './controllers/authController.js'
import cors from 'cors'

mongoose.connect("mongodb://localhost:27017/shop", { useUnifiedTopology: true, useNewUrlParser: true });

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors()); 

// app.use("/auth", authRouter);
app.use("/user", userRouter)  //authenticateJWT, 
// app.use("/category", authenticateJWT, categoryRouter);
// app.use("/product", authenticateJWT, productRouter);
// app.use("/cart", authenticateJWT, cartRouter);
// app.use("/order", authenticateJWT, orderRouter)

app.use("/", function (request, response) {
  response.send(`Main page`);
});

app.listen(3030);
