import express from 'express';
import { check } from 'express-validator'; 
import { createUser, deleteUser, updateUser, getUserById, getAllUsers } from '../controllers/userController.js';
// import { checkAccess } from '../controllers/authController.js';

const userRouter = express.Router();

userRouter.post("/create", createUser); //  [
//      check('email', "email field cannot be empty").notEmpty(),
//     check('password', "password length must be between 4 and 20 symbols").isLength({min:4, max: 20})
// ], checkAccess
userRouter.delete("/delete/:id", deleteUser); //checkAccess
userRouter.put("/:id", updateUser);
userRouter.get("/allUsers", getAllUsers);
userRouter.get("/:id", getUserById);

export default userRouter;